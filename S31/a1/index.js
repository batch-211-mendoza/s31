const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
	if (request.url == "/login"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("You are in the login page");
	} else {
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not available");

	}
});

server.listen(port);

console.log(`Server now accessible at localhost:${port}.`);

/*
	1. Open gitbash on a1 folder
	2. Enter "npm install -g nodemon"
	3. Enter "nodemon index.js"
*/