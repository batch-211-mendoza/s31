// a. What directive is used by Node.js in loading the modules it needs?
	// Answer: "require" directive

// b. What Node.js module contains a method for server creation?
	// Answer: "http" module

// c. What is the method of the http object responsible for creating a server using Node.js?
	// Answer: "createServer()" method

// d. What method of the response object allows us to set status codes and content types?
	// Answer: "writeHead()" method

// e. Where will console.log() output its contents when run in Node.js?
	// Answer: Server Terminal / Git Bash

// f. What property of the request object contains the address' endpoint?
	// Answer: request.url